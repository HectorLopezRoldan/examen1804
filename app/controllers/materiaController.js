const materia = require('../models/Materias')
const { ObjectId } = require('mongodb')

const index = (req, res) => {
    materia.find((err, materias) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo  la lista de productos'
      })
    }
    return res.json(materias)
  })
}

const show = (req, res) => {
    const id = req.params.id
    materia.findById(id, (err, mat) => {
      if (err) return res.status(500).json({ message: 'error' })
      if (!mat) return res.status(404).json({ message: 'not found' })
      return res.json(mat)
    })
  }

  const create = (req, res) => {
    const mat = new materia(req.body)
    mat.save((err, mat) => {
      if (err) {
        return res.status(400).json({
          message: 'Error al guardar el mat',
          error: err
        })
      }
      return res.status(201).json(mat)
    })
  }

  const update = (req, res) => {
    const id = req.params.id
    materia.findOne({ _id: id }, (err, mat) => {
      if (!ObjectId.isValid(id)) {
        return res.status(404).send()
      }
      if (err) {
        return res.status(500).json({
          message: 'Se ha producido un error al guardar mat',
          error: err
        })
      }
      if (!mat) {
        return res.status(404).json({
          message: 'No hemos encontrado mat'
        })
      }
  
      Object.assign(mat, req.body)
  
      mat.save((err, mat) => {
        if (err) {
          return res.status(500).json({
            message: 'Error al guardar mat'
          })
        }
        if (!mat) {
          return res.status(404).json({
            message: 'No hemos encontrado mat'
          })
        }
        return res.json(mat)
      })
    })
  }

  const destroy = (req, res) => {
    const id = req.params.id
    materia.findByIdAndDelete(id, (err, data) => {
      if (err) return res.status(500).json({ message: 'error' })
      return res.json(data)
    })
  }
  

module.exports = { index,show ,create,update,destroy}