const connection = require('../config/mongo')
const user = require('../models/Users')
const servicejwt=require('../services/servicejwt')
 


  function login(req, res) {
    const name = req.params.nombre
    const pass = req.params.password
    user.find({ nombre : name }, (err, userF) => {
      if (err) return res.status(500).json({ message: 'error' })
      if (!userF) return res.status(404).json({ message: 'not found' })
      if(userF.password == pass){
        return res.status(200).send({userF, token: servicejwt.createToken(userF) })
      }
    })
 }

 const index = (req, res) => {
    user.find((err, users) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo  la lista users'
      })
    }
    return res.json(users)
  })
}
 

module.exports = {login,index }