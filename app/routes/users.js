const express = require('express')

// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const userController = require('../controllers/userController')
//const auth=require('../../middleware/auth')


//router.use(auth.auth);

router.post('/login', (req, res) => {
    userController.login(req, res)
})

module.exports = router