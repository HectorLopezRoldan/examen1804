const express = require('express')

// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const materiaController = require('../controllers/materiaController')
const userController = require('../controllers/userController')
var middleware = require('../auth');
router.get('/', (req, res) => {
    materiaController.index(req, res)
})

router.get('/:id',  (req, res) => {
    materiaController.show(req, res)
  })

router.post('/', (req, res) => {
    materiaController.create(req, res)
  })

  router.put('/:id', (req, res) => {
    materiaController.update(req, res)
  })

  router.delete('/:id', (req, res) => {
    materiaController.destroy(req, res)
  })

  router.get('/privada',middleware.auth, (req, res) => {
    userController.index(req, res)
  })
  
  

module.exports = router