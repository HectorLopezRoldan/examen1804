const mongoose = require('mongoose')
const Schema = mongoose.Schema
// cifrado de passwords.
const bcrypt = require('bcrypt-nodejs')

const materiaSchema = new Schema({
    codigo: { type: Number, required: true, max: 500 },
    nombre: { type: String, maxlength: 255 },
    curso: { type: String, maxlength: 255 },
    horas: { type: Number, required: true, max: 20 }
})

//const materia = mongoose.model('Materia', materiaSchema)

module.exports = mongoose.model('Materia', materiaSchema)
