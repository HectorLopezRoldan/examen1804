const mongoose = require('mongoose')
const Schema = mongoose.Schema
// cifrado de passwords.
const bcrypt = require('bcrypt-nodejs')

const userSchema = new Schema({
    codigo: { type: Number, required: true, max: 5 },
    nombre: { type: String, maxlength: 255 },
    password: { type: String, maxlength: 255 },
  
})
//const user = mongoose.model('User', userSchema)

module.exports = mongoose.model('User', userSchema)

